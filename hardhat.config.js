const fs = require("fs");

require("@nomiclabs/hardhat-solhint");
require("@nomiclabs/hardhat-truffle5");
require("solidity-coverage");
require("hardhat-gas-reporter");
require("hardhat-deploy");
require("hardhat-deploy-ethers");
require("@openzeppelin/hardhat-upgrades");
require("@nomiclabs/hardhat-etherscan");

const ONE_GWEI = 1000000000;

const DEV_MNEMONIC = fs.existsSync("./dev.key")
  ? fs.readFileSync("./dev.key", { encoding: "utf8" })
  : "";
const PROD_MNEMONIC = fs.existsSync("./prod.key")
  ? fs.readFileSync("./prod.key", { encoding: "utf8" })
  : "";

const ALCHEMY_KEY = fs.existsSync("./alchemy.key")
  ? fs.readFileSync("./alchemy.key", { encoding: "utf8" })
  : "";
const ETHERSCAN_KEY = fs.existsSync("./etherscan-polygon.key")
  ? fs.readFileSync("./etherscan-polygon.key", { encoding: "utf8" })
  : "";

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  networks: {
    hardhat: {
      accounts: {
        mnemonic: DEV_MNEMONIC,
        initialIndex: 0,
        count: 10,
      },
      blockGasLimit: 30_000_000,
    },
    localhost: {
      accounts: {
        mnemonic: DEV_MNEMONIC,
        initialIndex: 0,
        count: 10,
      },
      blockGasLimit: 30_000_000,
    },
    mumbai: {
      url: "https://rpc-mumbai.maticvigil.com",
      accounts: {
        mnemonic: DEV_MNEMONIC,
        initialIndex: 0,
        count: 10,
      },
      network_id: 80001,
    },
    polygon: {
      url: `https://polygon-mainnet.g.alchemy.com/v2/${ALCHEMY_KEY}`,
      accounts: {
        mnemonic: PROD_MNEMONIC,
        initialIndex: 0,
        count: 10,
      },
      gasPrice: 70 * ONE_GWEI,
      network_id: 137,
      timeout: 750000,
    },
  },
  solidity: {
    compilers: [
      {
        version: "0.8.17",
        settings: {
          optimizer: {
            enabled: true,
            runs: 5000,
          },
        },
      },
    ],
  },
  watcher: {
    compile: {
      tasks: ["compile"],
      files: ["./contracts"],
      verbose: true,
    },
    test: {
      tasks: ["test"],
      files: ["./test/*.js", "./contracts"],
    },
    node: {
      tasks: ["node"],
      files: ["./scripts/*.js", "./contracts"],
    },
  },
  namedAccounts: {
    deployer: {
      default: 0,
    },
  },
};
