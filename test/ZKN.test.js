require("@nomiclabs/hardhat-truffle5");
const { artifacts } = require("hardhat");
const { expect } = require("chai");
const { expectRevert, BN, expectEvent } = require("openzeppelin-test-helpers");

const ZKN = artifacts.require("ZKN");

contract("ZKN", ([deployer, operator, other]) => {
  const initialSupply = new BN("3041044000000000000000000");
  let znk, operatorRole;

  beforeEach(async () => {
    znk = await ZKN.new();

    await znk.initialize(operator);

    operatorRole = await znk.OPERATOR_ROLE();
  });

  describe("when initializing", () => {
    it("should set all operator role to operator address", async () => {
      expect(await znk.hasRole(operatorRole, operator)).to.be.true;
    });

    it("should not allow initalize a second time", async () => {
      await expectRevert(
        znk.initialize(operator),
        "Initializable: contract is already initialized"
      );
    });
    it("should mint corrent amount", async () => {
      expect(await znk.balanceOf(operator)).to.be.a.bignumber.eq(initialSupply);
    });
  });
});
