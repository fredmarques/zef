require("@nomiclabs/hardhat-truffle5");
const { artifacts } = require("hardhat");
const { expect } = require("chai");
const { expectRevert, BN, expectEvent } = require("openzeppelin-test-helpers");

const ZKN = artifacts.require("ZKN");
const ZEF = artifacts.require("ZEF");

contract("ZEF", ([deployer, operator, owner, client1, other]) => {
  const initialSupply = new BN("3041044000000000000000000");
  let znk, zef, ownerRole, memberRole;

  beforeEach(async () => {
    znk = await ZKN.new();
    zef = await ZEF.new();

    const contract = await znk.initialize(operator);
    await zef.initialize(owner, znk.address);

    ownerRole = await zef.OWNER_ROLE();
    memberRole = await zef.MEMBER_ROLE();
  });

  describe("when initializing", () => {
    it("should set OWNER role to owner address", async () => {
      expect(await zef.hasRole(ownerRole, owner)).to.be.true;
    });

    it("should not allow initalize a second time", async () => {
      await expectRevert(
        znk.initialize(operator),
        "Initializable: contract is already initialized"
      );
    });
    it("should mint corrent amount", async () => {
      expect(await znk.balanceOf(operator)).to.be.a.bignumber.eq(initialSupply);
    });
  });
});
