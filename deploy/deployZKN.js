const { ethers, upgrades } = require("hardhat");

const operator = "";
const owner = "";

module.exports = async ({ getNamedAccounts }) => {
  const { deployer } = await getNamedAccounts();

  const ZKN = await ethers.getContractFactory("ZKN");
  const factory = await upgrades.deployProxy(Factory, [operator], {
    from: deployer,
    timeout: 0,
  });

  await factory.deployed();

  console.log("Upgradeable ZKN deployed to:", factory.address);
};

module.exports.tags = ["zkn"];

// hh deploy --network mumbai --tags
