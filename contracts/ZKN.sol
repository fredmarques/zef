// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/draft-ERC20PermitUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

contract ZKN is
    Initializable,
    ERC20Upgradeable,
    AccessControlUpgradeable,
    ERC20PermitUpgradeable
{
    bytes32 public constant OPERATOR_ROLE = keccak256("OPERATOR_ROLE");

    function initialize(address _operator) public initializer {
        __ERC20_init("ZEF Kuna", "ZKN");
        __AccessControl_init();
        __ERC20Permit_init("ZEF Kuna");

        _grantRole(DEFAULT_ADMIN_ROLE, _operator);
        _grantRole(OPERATOR_ROLE, _operator);

        _mint(_operator, 3041044 * 10**decimals());
    }
}
