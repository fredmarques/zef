// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/draft-ERC20PermitUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlEnumerableUpgradeable.sol";

contract ZEF is Initializable, ReentrancyGuardUpgradeable, AccessControlEnumerableUpgradeable {
	bytes32 public constant OWNER_ROLE = keccak256("OWNER_ROLE");
	bytes32 public constant MEMBER_ROLE = keccak256("MEMBER_ROLE");

	IERC20Upgradeable private ZKNToken;
	uint256 numProjects;

	mapping(uint256 => Project) public projects;

	struct Project {
		string name;
		string description;
		address owner;
		bool created;
		uint256 numFunders;
		uint256 amount;
		mapping(address => uint256) funders;
	}

	modifier onlyMember() {
		require(hasRole(MEMBER_ROLE, msg.sender), "sender is not a MEMBER");
		_;
	}
	modifier onlyOwner() {
		require(hasRole(OWNER_ROLE, msg.sender), "sender is not a OWNER");
		_;
	}

	event ProjectCreated(uint256 projectId, string name, string description, address owner);
	event ProjectFunded(address funder, uint256 projectId, uint256 amount);

	function initialize(address _owner, IERC20Upgradeable _ZKNToken) public initializer {
		__AccessControl_init();

		_grantRole(DEFAULT_ADMIN_ROLE, _owner);
		_grantRole(OWNER_ROLE, _owner);

		ZKNToken = _ZKNToken;
	}

	function join() external {
		_grantRole(MEMBER_ROLE, msg.sender);
	}

	function isMember(address _wallet) external view returns (bool) {
		return hasRole(MEMBER_ROLE, _wallet);
	}

	function memberFee(address member) external payable onlyMember {
		require(hasRole(MEMBER_ROLE, msg.sender), "not a MEMBER");

		ZKNToken.transferFrom(msg.sender, member, msg.value);
	}

	function createProject(string calldata _name, string calldata _description) external onlyMember {
		Project storage p = projects[numProjects++];
		p.name = _name;
		p.description = _description;
		p.created = true;

		emit ProjectCreated(numProjects, _name, _description, msg.sender);
	}

	function invest(uint256 _projectId, uint256 _amount) external onlyMember {
		require(ZKNToken.balanceOf(msg.sender) >= _amount, "not enough funds");
		require(projects[_projectId].created, "project not created");

		Project storage p = projects[_projectId];

		if (p.funders[msg.sender] == 0) {
			p.numFunders += 1;
		}
		p.funders[msg.sender] += _amount;

		ZKNToken.transferFrom(msg.sender, p.owner, _amount);

		emit ProjectFunded(msg.sender, _projectId, _amount);
	}

	function withdraw(uint256 _amount, address payable _member) external onlyMember {
		require(ZKNToken.balanceOf(msg.sender) >= _amount, "not enough funds");
		require(msg.sender == _member, "requires a member address ");

		ZKNToken.transferFrom(msg.sender, address(this), _amount);
		_member.transfer(_amount);
	}
}
